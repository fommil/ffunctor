FROM haskell:8.4.4

# docker build . -t registry.gitlab.com/fommil/ffunctor:8.4.4
# docker login --username=fommil registry.gitlab.com
# docker push registry.gitlab.com/fommil/ffunctor:8.4.4

# add --no-cache to build to force a new cabal update

ENV GITLAB_USER=fommil
ENV GITLAB_PROJECT=ffunctor

# structured to increase the chance of cache hits
RUN cabal v2-update &&\
    mkdir -p /builds/${GITLAB_USER} && cd /builds/${GITLAB_USER} &&\
    git clone https://gitlab.com/${GITLAB_USER}/${GITLAB_PROJECT}.git &&\
    cd ${GITLAB_PROJECT} &&\
    cabal v2-build all --only-dependencies --enable-test &&\
    cd / && rm -rf /builds

# hopefully this is the only delta needed when updating deps
COPY ${GITLAB_PROJECT}.cabal /builds/${GITLAB_USER}/${GITLAB_PROJECT}/
RUN cd /builds/${GITLAB_USER}/${GITLAB_PROJECT} &&\
    cabal v2-build all --only-dependencies --enable-test &&\
    cd / && rm -rf /builds
